﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAplicationTest.Models
{
    public class Permission
    {
        [Key, DisplayName("Id")]
        public int Id { get; set; }
        [StringLength(128), Required, DisplayName("Name Employee")]
        public string NameEmployee { get; set; }
        [StringLength(128), Required, DisplayName("Last Name Employee")]
        public string LastNameEmployee { get; set; }
        [ForeignKey("PermissionTypeId"),Required, DisplayName("Permission Type")]
        public int PermissionTypeId { get; set; }
        [Required, DisplayName("Permission Date"), DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime PermissionDate { get; set; }

        public virtual PermissionType PermissionType { get; set; }

    }
}
