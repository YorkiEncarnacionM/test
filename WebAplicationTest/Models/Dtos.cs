﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAplicationTest.Models
{
    public class Dtos
    {
        public int Id { get; set; }

        public string NameEmployee { get; set; }

        public string LastNameEmployee { get; set; }

        public int PermissionTypeId { get; set; }

        public string PermissionDate { get; set; }

        public string Description { get; set; }
    }
}
