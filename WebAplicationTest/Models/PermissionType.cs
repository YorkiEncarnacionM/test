﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAplicationTest.Models
{
    public class PermissionType
    {
        [Key, DisplayName("Id")]
        public int PermissionTypeId { get; set; }
        [StringLength(128), Required, DisplayName("Description")]
        public string Description { get; set; }
    }
}
