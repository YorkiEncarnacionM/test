﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebAplicationTest.Models;

namespace WebAplicationTest.Context
{
    public class ContextBD: DbContext
    {

        public ContextBD()
        {
        }

        public ContextBD(DbContextOptions<ContextBD> options): base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
          

        }

        public DbSet<Permission> Permission { get; set; }
        public DbSet<PermissionType> PermissionType { get; set; }

    }
}
